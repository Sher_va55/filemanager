package com.ppjava13v2.vera.filemanager_04102015;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;
import java.util.List;


public class FileAdapter  extends ArrayAdapter <File> {

    private Context context;
    private List<File> objects;

    public FileAdapter(Context context, int resource, List<File> objects) {
        super(context, resource, objects);
        this.context=context;
        this.objects=objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View currentView;
        currentView=
                convertView==null?
                        LayoutInflater.from(context).inflate (R.layout.viewforlist,null):
                        convertView;

        TextView textView = (TextView) currentView.findViewById(R.id.foldername);
        ImageButton button = (ImageButton) currentView.findViewById(R.id.folder);

        textView.setText(objects.get(position).toString());

        if (objects.get(position).isDirectory()) {
            button.setImageResource(R.drawable.folder);

        }
        else {
            button.setImageResource(R.drawable.files);
        }



        return currentView;
    }
}
