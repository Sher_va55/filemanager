package com.ppjava13v2.vera.filemanager_04102015;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


public class FragmentButtons  extends Fragment implements View.OnClickListener {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate (R.layout.fragmentbuttons,null);
        view.setBackgroundColor(Color.GRAY);

        ImageButton buttonBack = (ImageButton) view.findViewById(R.id.back);
        ImageButton buttonListView = (ImageButton) view.findViewById(R.id.listview);
        ImageButton buttonPictures = (ImageButton) view.findViewById(R.id.pictures);
        ImageButton buttonPaste = (ImageButton) view.findViewById(R.id.paste);
        ImageButton buttonDelete = (ImageButton) view.findViewById(R.id.delete);
        ImageButton buttonCopy = (ImageButton) view.findViewById(R.id.copy);

        buttonListView.setOnClickListener(this);
        buttonPaste.setOnClickListener(this);
        buttonPictures.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
        buttonCopy.setOnClickListener(this);
        buttonBack.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        System.out.println("Hello!!!");

    }
}
