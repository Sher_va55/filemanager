package com.ppjava13v2.vera.filemanager_04102015;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends Activity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.fragmentlistview);
        FileAdapter  adapter = new FileAdapter (this,R.layout.viewforlist,getAllFiles ());
        listView.setAdapter(adapter);
    }

    public List<File>  getAllFiles () {

        File  baseDir = Environment.getExternalStorageDirectory();
        File fileNames []=baseDir.listFiles();
        System.out.println(fileNames.length);
        List<File> fileNamesList=new ArrayList<File>(Arrays.asList(fileNames));

        return fileNamesList;
    }


}
